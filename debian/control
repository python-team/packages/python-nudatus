Source: python-nudatus
Section: python
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 pandoc,
 python3-all,
 python3-pytest,
 python3-setuptools,
Standards-Version: 4.6.2
Homepage: https://github.com/ZanderBrown/nudatus
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-nudatus
Vcs-Git: https://salsa.debian.org/python-team/packages/python-nudatus.git
Rules-Requires-Root: no

Package: python3-nudatus
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
Description: module to remove comments from Python 3 scripts
 Nudatus was created to help fit longer, heavily-documented Python programs
 onto the micro:bit single-board computer (SBC). It should be suitable for
 similar platforms with restricted storage capacity.
 .
 Although the library is designed to be embedded, this package also provides
 the `nudatus` CLI utility.
 .
 Nudatus uses the tokenizer built into Python, so only supports the syntax of
 the version of Python it's running on.
 .
 This package installs the library and CLI utility for Python 3.
